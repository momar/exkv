package exkv

// Index adds an index alias to an entry. If there's e.g. an entry with the name "example", and it has "email" set to
// "mail@example.org", you can use cache.Index("example","email","mail@example.org) to make it available later with
// cache.GetByIndex("email","mail@example.org"). Previous index aliases will be overwritten.
func (cache *Cache) Index(name string, index string, value string) {
	cache.lockIndices.Lock()
	defer cache.lockIndices.Unlock()

	if cache.indices == nil {
		cache.indices = map[string]map[string]string{}
	}
	if cache.indices[index] == nil {
		cache.indices[index] = map[string]string{}
	}
	if oldEntry, ok := cache.indices[index][value]; ok &&
		cache.entries[oldEntry] != nil &&
		cache.entries[oldEntry].indices != nil &&
		cache.entries[oldEntry].indices[index] == value {
		// Remove previous index from the entry
		delete(cache.entries[oldEntry].indices, index)
	}
	cache.indices[index][value] = name

	if cache.entries[name].indices == nil {
		cache.entries[name].indices = map[string]string{}
	}
	cache.entries[name].indices[index] = value
}

// autoIndex calls cache.Indexer on an entry and adds the indices using cache.Index
func (cache *Cache) autoIndex(name string, entry *cacheEntry) {
	if cache.Indexer == nil {
		return
	}

	indices := cache.Indexer(name, entry)
	if indices == nil {
		return
	}
	for index, value := range indices {
		cache.Index(name, index, value)
	}
}

// GetByIndex retrieves an entry's name and value from the cache if it exists & is valid, using an index alias instead
// of the entry's name directly. If the index, the index value, or the entry doesn't exist, it returns nil as a value.
func (cache *Cache) GetByIndex(index string, value string) (string, interface{}) {
	cache.lockIndices.RLock()
	defer cache.lockIndices.RUnlock()

	if cache.indices == nil || cache.indices[index] == nil {
		return "", nil
	} else if name, ok := cache.indices[index][value]; !ok {
		return "", nil
	} else {
		return name, cache.Get(name)
	}
}

// GetAndRenewByIndex retrieves an entry's name and value from the cache if it exists & is valid, using an index alias instead
// of the entry's name directly. If the index, the index value, or the entry doesn't exist, it returns nil as a value.
// Additionally, it renews the expiration of the entry to the current time + the entry's expiration timeout (analog to GetAndRenew).
func (cache *Cache) GetAndRenewByIndex(index string, value string) (string, interface{}) {
	cache.lockIndices.RLock()
	defer cache.lockIndices.RUnlock()

	if cache.indices == nil || cache.indices[index] == nil {
		return "", nil
	} else if name, ok := cache.indices[index][value]; !ok {
		return "", nil
	} else {
		return name, cache.GetAndRenew(name)
	}
}

// delete removes an entry and all its index aliases from the cache, and must be used in place of delete(cache.entries,name).
func (cache *Cache) delete(name string) {
	cache.lockIndices.Lock()
	defer cache.lockIndices.Unlock()
	if e := cache.entries[name]; e != nil && e.indices != nil {
		for index, value := range e.indices {
			if cache.indices != nil && cache.indices[index] != nil {
				if cache.indices[index][value] != name {
					continue
				}
				// We've got an existing index that's referring to the current item
				delete(cache.indices[index], value)
				if len(cache.indices[index]) <= 0 {
					delete(cache.indices, index)
				}
				if len(cache.indices) <= 0 {
					cache.indices = nil
				}
			}
		}
	}
	// Use the normal delete function to delete the entry itself
	delete(cache.entries, name)
}
