package exkv

import "time"

// All is a filter function that includes all entries.
var All CacheFilter = func(string, interface{}) bool {
	return true
}

// Filter returns a set of entries from the cache, filtered by a function that returns true if the entry (consisting of
// name & value) should be included. Note that it can be really slow for large caches!
func (cache *Cache) Filter(fn CacheFilter) map[string]interface{} {
	result := map[string]interface{}{}
	now := time.Now()
	for name, entry := range cache.entries {
		if !(entry.timeout > 0 && !now.Before(entry.created.Add(entry.timeout))) {
			// The entry is valid
			if fn(name, entry.value) {
				// The entry is included by the filter
				result[name] = entry.value
			}
		}
	}
	return result
}
