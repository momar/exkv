package exkv

import (
	"sync"
	"time"
)

////////////////
// DEFINITION //
////////////////

// Forever can be used as an expiration timeout to make entries valid forever.
const Forever time.Duration = -1

// CacheIndexer is a function that maps a name and value of an entry to its index alias values (map[index]value).
type CacheIndexer func(string, interface{}) map[string]string

// CacheFilter is a function that returns true if the entry (consisting of name & value) should be included by Cache.Filter.
type CacheFilter func(string, interface{}) bool

// Cache is an in-memory key-value store with expiration handling & automatic cleanups.
type Cache struct {
	DefaultTimeout  time.Duration // Default expiration timeout for new entries created with cache.Set(...), or a negative numbers to make entries valid forever; defaults to 30 seconds
	CleanupInterval time.Duration // Minimum interval for automatic cleanups (which will be triggered on any operation on the Cache); the default value 0 disables automatic cleanups
	Indexer         CacheIndexer  // Function that maps a name and value of an entry to its index alias values (map[index]value)

	entries     map[string]*cacheEntry       // Map of entries stored in the cache
	indices     map[string]map[string]string // Map of index -> index value -> entry name, to refer to entries by aliases.
	lastCleanup time.Time                    // Time of the last cleanup

	lockCleanup     sync.Mutex   // Lock to make sure that two cleanups are never running at the same time
	lockAutoCleanup sync.Mutex   // Lock to make sure that two auto-cleanup checks are never running at the same time (if there are multiple simultaneous cache operations, this makes sure that only one cleanup will be scheduled per CleanupInterval)
	lockIndices     sync.RWMutex // Lock to make sure that the indices have a consistent state
}

// cacheEntry represents an entry in a Cache
type cacheEntry struct {
	created time.Time         // Creation (or renewal) time of the entry
	timeout time.Duration     // Expiration timeout of the entry
	value   interface{}       // The cached value itself
	indices map[string]string // index->value map for quick access
}

/////////
// SET //
/////////

// Set stores a value in the Cache, using the default timeout of the Cache. Setting the value to nil deletes the entry.
func (cache *Cache) Set(name string, value interface{}) {
	// defer cache.autoCleanup() handled by cache.SetWithTimeout()
	cache.SetWithTimeout(name, value, 0)
}

// SetWithTimeout stores a value in the Cache, using a custom timeout. Setting the value to nil deletes the entry.
func (cache *Cache) SetWithTimeout(name string, value interface{}, timeout time.Duration) {
	defer cache.autoCleanup()
	if cache.entries == nil {
		// Initialize the entry map
		cache.entries = map[string]*cacheEntry{}
	}
	if value == nil {
		// Delete the entry if the value == nil
		cache.delete(name)
		return
	}
	if timeout == 0 {
		if int64(cache.DefaultTimeout) == 0 {
			// Set DefaultTimeout to its default value
			cache.DefaultTimeout = 30 * time.Second
		}
		// Set the timeout to the cache's default value
		timeout = cache.DefaultTimeout
	}
	// Store the entry
	entry := &cacheEntry{
		created: time.Now(),
		timeout: timeout,
		value:   value,
	}
	cache.entries[name] = entry
	cache.autoIndex(name, entry)
}

/////////
// GET //
/////////

// entry retrieves a cacheEntry from the Cache if it exists & is still valid, otherwise it deletes the entry & returns nil.
func (cache *Cache) entry(name string) *cacheEntry {
	defer cache.autoCleanup()
	if cache.entries == nil {
		return nil // The map doesn't exist yet
	} else if entry, ok := cache.entries[name]; !ok {
		return nil // There is no such entry
	} else if entry.timeout > 0 && !time.Now().Before(entry.created.Add(entry.timeout)) {
		cache.delete(name)
		return nil // The entry has expired
	} else {
		return entry // Valid entry! \o/
	}
}

// Get retrieves an entry from the cache if it exists & is valid, otherwise it returns nil.
func (cache *Cache) Get(name string) interface{} {
	// defer cache.autoCleanup() handled by cache.entry()
	var entry = cache.entry(name)
	if entry == nil {
		return nil
	}
	return entry.value
}

// GetAndRenew retrieves an entry from the cache if it exists & is valid, otherwise it returns nil.
// Additionally, it renews the expiration of the entry to the current time + the entry's expiration timeout.
func (cache *Cache) GetAndRenew(name string) interface{} {
	// defer cache.autoCleanup() handled by cache.entry()
	var entry = cache.entry(name)
	if entry == nil {
		return nil
	}
	entry.created = time.Now()
	return entry.value
}
