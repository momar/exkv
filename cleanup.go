package exkv

import "time"

// Cleanup removes expired entries from the cache.
// It will be called automatically on every cache operation if CleanupInterval is set no a non-zero value.
func (cache *Cache) Cleanup() {
	cache.lockCleanup.Lock()
	defer cache.lockCleanup.Unlock()

	now := time.Now()
	for name, entry := range cache.entries {
		if entry.timeout > 0 && !now.Before(entry.created.Add(entry.timeout)) {
			// The entry has expired
			cache.delete(name)
		}
	}
}

// autoCleanup checks if there was no cleanup in the last CleanupInterval, and triggers one if required.
func (cache *Cache) autoCleanup() {
	if cache.CleanupInterval != time.Duration(0) {
		cache.lockAutoCleanup.Lock()
		defer cache.lockAutoCleanup.Unlock()

		if cache.CleanupInterval != time.Duration(0) && time.Now().Before(cache.lastCleanup.Add(cache.CleanupInterval)) {
			// lastCleanup was not at least CleanupInterval ago, no cleanup needed yet
			return
		}
		// A cleanup is required, so start it!
		cache.lastCleanup = time.Now()
		go cache.Cleanup()
	}
}
