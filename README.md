# exkv Key-Value Cache

[![](https://godoc.org/codeberg.org/momar/exkv?status.svg)](https://godoc.org/codeberg.org/momar/exkv)

`exkv.Cache` is an in-memory key-value store with expiration handling & automatic cleanups. It also supports index
aliases, making it easy to reference a single entry by multiple keys (e.g. a user by its username or its email address).

## Basic Usage Example

```go
var myCache = &exkv.Cache{
	DefaultTimeout: 5 * time.Minute,
	CleanupInterval: 30 * time.Minute,
	Indexer: func(name string, value interface{}) map[string]string {
		return map[string]string{
			"email": value.(myCacheItem).Email,
		}
	},
}

type myCacheEntry struct {
	Username string
	Email string
	IsAdmin bool
}


// Writing to the cache:
myCache.Set("example", myCacheEntry{ "example", "mail@example.org", false })
myCache.SetWithTimeout("homer", myCacheEntry{ "homer", "homer@example.org", true }, exkv.Forever)
myCache.SetWithTimeout("linda", myCacheEntry{ "linda", "linda@example.org", true }, 2 * time.Second)

time.Sleep(5 * time.Second)
println(myCache.Get("linda")) // nil


// Retrieve some information either from the cache, or from a complex query:
var username = "example"
if e := myCache.Get(username); e != nil {
	return e.(myCacheEntry)
}
var user myCacheEntry
// TODO: Do very complicated stuff to retrieve the user details ...
myCache.Set(username, user)


// Get a list of all admins:
var users = myCache.Filter(func(name string, value interface{}) bool {
	return value.(myCacheEntry).IsAdmin
})
println(users["homer"].Username) // homer


// Use the index to retrieve a user, and reset the expiration:
var name, user = myCache.GetAndRenewByIndex("email", "homer@example.org")
println(name) // email
println(user.IsAdmin) // true


// Delete an entry manually:
myCache.Set("example", nil)
```

For advanced documentation, please refer to the [GoDoc documentation](https://godoc.org/codeberg.org/momar/exkv).
